import { gDevcampReact, gTyLeSinhVien } from "./info";
import logo from "./assets/images/logo-og.png";

function App() {
  return (
    <div className="App">
      <h1>{gDevcampReact.title}</h1>
      <img src={logo} width="500px" />
      <p>Tỷ lệ sinh viên đang học: {gTyLeSinhVien()}%</p>
      <p>{gTyLeSinhVien() > 15 ? "Sinh viên theo học nhiều" : "Sinh viên theo học ít"}</p>
      <ul>
        {
          gDevcampReact.benefits.map((value, index) => {
            return <li key={index}>{value}</li>
          })
        }
      </ul>
    </div>
  );
}

export default App;
